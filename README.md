# VaporMap

--- 

## Description

VaporMap est une application web permettant de référencer des points GPS dans une base de données. 
Ces points peuvent être affichés sur une carte.

Techniquement, c'est une application écrite en python, qui utilise le framework Flask.

>Cette application est une maquette développée rapidement dans un but pédagogique par Tristan Le Toullec.


---


# Installation


## Cloner le projet


```bash
git clone https://gitlab.imt-atlantique.fr/k22mobsi/vapormap_cicd.git
```

## Lancer le projet avec docker-compose file

Se déplacer vers le répertoire qui contient le fichier docker-compose:

```bash
cd vapormap-cicd
```

```bash
cd vapormap-app
```
Lancer le build de l'application :

```bash
COMPOSE_FILE=docker-compose.yml docker-compose build
```
Lancer l'application:

```bash
COMPOSE_FILE=docker-compose.yml docker-compose up -d
```
Visiter l'URL:  http://localhost:8000/

Stoper l'application:

```bash
COMPOSE_FILE=docker-compose.yml docker-compose down
```


## Lancer le déploiement du projet hors chaine CI/CD

Se déplacer vers le répertoire qui contient le fichier docker-compose du déploiement:

```bash
cd vapormap-cicd
```

```bash
cd vapormap-app
```
Se connecter au gitlab registry:

```bash
docker login -u gitlab-ci-token  -p glpat-zx5aV5-toDEorJMgv_ds gitlab-registry.imt-atlantique.fr/k22mobsi/vapormap_cicd
```
Lancer le build de l'application :

```bash
COMPOSE_FILE=docker-compose-dep.yml docker-compose build
```
Visiter l'URL:  http://localhost:8000/

Stoper l'application:

```bash
COMPOSE_FILE=docker-compose-dep.yml docker-compose down
```


