#!/bin/sh
sleep 10
flask db upgrade
gunicorn --bind 0.0.0.0:7000 wsgi:app
exec "$@"
